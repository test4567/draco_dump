'use_strict';

const fs = require('fs');
const assert = require('assert');
const draco3d = require('draco3d');
const decoderModule = draco3d.createDecoderModule({});

function decodeDracoData(rawBuffer, decoder) {
    const buffer = new decoderModule.DecoderBuffer();
    buffer.Init(new Int8Array(rawBuffer), rawBuffer.byteLength);
    const geometryType = decoder.GetEncodedGeometryType(buffer);

    let dracoGeometry = new decoderModule.Mesh();
    let status = decoder.DecodeBufferToMesh(buffer, dracoGeometry);
    
    decoderModule.destroy(buffer);

    return dracoGeometry;
}

function dumpAttrinte(decoderModule, decoder, dracoGeometry, attribute){
    var numComponents = attribute.num_components();
    var numPoints = dracoGeometry.num_points();
    var numValues = numPoints * numComponents;
    
    var attributeData = new decoderModule.DracoFloat32Array();
        decoder.GetAttributeFloatForAllPoints(dracoGeometry, attribute, attributeData);

        var count = 0;
        for(var i = 0; i < numValues; i+=numComponents)
        {
            var str = "" + count + ":";
            for(var j = 0; j < numComponents; j++)
            {
                str += attributeData.GetValue(i+j) + ",";
            }
            console.log(str);
            count++;
        }
}


var dracoBinFileName = process.argv[2];

fs.readFile(dracoBinFileName, function(err, data) {
    if (err) {
      return console.log(err);
    }
    console.log("Decoding file of size " + data.byteLength + " ..");
    // Decode mesh
    const decoder = new decoderModule.Decoder();
    const decodedGeometry = decodeDracoData(data, decoder);
    
    var attriute;

    // position
    console.log("position");
    attriute = decoder.GetAttribute(decodedGeometry, 0);
    dumpAttrinte(decoderModule, decoder, decodedGeometry, attriute);
    
    // normal
    console.log("\nnormal");
    attriute = decoder.GetAttribute(decodedGeometry, 2);
    dumpAttrinte(decoderModule, decoder, decodedGeometry, attriute);
    
    // uv
    console.log("\nuv");
    attriute = decoder.GetAttribute(decodedGeometry, 1);
    dumpAttrinte(decoderModule, decoder, decodedGeometry, attriute);
    
    // tangent
    console.log("\ntangent");
    attriute = decoder.GetAttribute(decodedGeometry, 3);
    dumpAttrinte(decoderModule, decoder, decodedGeometry, attriute);
    
    decoderModule.destroy(decoder);
    decoderModule.destroy(decodedGeometry);
  });

